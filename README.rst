========
cli-args
========


.. image:: https://img.shields.io/pypi/v/cli_args.svg
        :target: https://pypi.python.org/pypi/cli_args

.. image:: https://img.shields.io/travis/konrad-kocik/cli_args.svg
        :target: https://travis-ci.org/konrad-kocik/cli_args

.. image:: https://readthedocs.org/projects/cli-args/badge/?version=latest
        :target: https://cli-args.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




Decorator for passing CLI arguments into function's parameters.


* Free software: MIT license
* Documentation: https://cli-args.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
