from cli_args import cli


def test_decorated_func_returns_correct_result_ut():
    expected_result = 2

    @cli
    def decorated_func():
        return expected_result

    assert decorated_func() == expected_result


def test_decorated_func_uses_positional_arg_ut():
    expected_result = "a"

    @cli
    def decorated_func(arg):
        return arg

    assert decorated_func(expected_result) == expected_result


def test_decorated_func_uses_positional_args_ut():
    first_param = 6
    second_param = 7
    expected_result = first_param + second_param

    @cli
    def decorated_func(first_arg, second_arg):
        return first_arg + second_arg

    assert decorated_func(first_param, second_param) == expected_result


def test_decorated_func_uses_keyword_arg_ut():
    expected_result = 3.14

    @cli
    def decorated_func(arg):
        return arg

    assert decorated_func(arg=expected_result) == expected_result


def test_decorated_func_uses_keyword_args_ut():
    first_param = [1]
    second_param = [2]
    expected_result = first_param + second_param

    @cli
    def decorated_func(first_arg, second_arg):
        return first_arg + second_arg

    assert decorated_func(first_arg=first_param, second_arg=second_param) == expected_result


def test_decorated_func_uses_positional_arg_and_keyword_arg_ut():
    first_param = "foo"
    second_param = 10
    expected_result = first_param * second_param

    @cli
    def decorated_func(first_arg, second_arg):
        return first_arg * second_arg

    assert decorated_func(first_param, second_arg=second_param) == expected_result


def test_decorated_func_uses_positional_args_and_keyword_args_ut():
    first_param = 100
    second_param = 9
    third_param = 6
    fourth_param = 3
    expected_result = first_param - second_param - third_param - fourth_param

    @cli
    def decorated_func(first_arg, second_arg, third_arg, fourth_arg):
        return first_arg - second_arg - third_arg - fourth_arg

    assert decorated_func(first_param, second_param, third_arg=third_param, fourth_arg=fourth_param) == expected_result
